module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		webpack: {
			main: {
				entry: __dirname + "/src/index.js",
				output: {
					path: __dirname + "/",
					filename: "compiled.js",
				},
				module: {
					loaders: [
						{ test: /\.jsx?$/, loader: "jsx-loader" }
					],
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-webpack');

	// Default task(s).
	grunt.registerTask('default', ['webpack']);
};