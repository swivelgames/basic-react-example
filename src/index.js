/** @jsx React.DOM */
var React = require('react');

var Box = React.createClass({
	getInitialState: function(){
		return {
			count: 0
		};
	},
	increaseClickCount: function(){
		this.setState({
			count: this.state.count + 1
		});
	},
	render: function(){
		return (
			<div>
				<h1>Hello World!</h1>

				<p>
					Lorem ipsum dolor sit amit.
				</p>

				<p>
					You&apos;ve clicked the button: {this.state.count} times
				</p>

				<button onClick={this.increaseClickCount}>Click Me</button>
			</div>
		);
	}
});

/**
 * @TODO:
 * Create a new react component that changes its background color by clicking a button
 */

React.render(<Box />, document.getElementById('myBox'));